<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;

?>

<div class="container">

    <?php $form = ActiveForm::begin(['options'=>['class'=>'form-horizontal', 'id'=>'product', 'enctype' => 'multipart/form-data', 'method' =>'post']]) ?>
    <?php

        echo $form->field($model, 'name')->label('Наименование');
        echo $form->field($model, 'desc')->label('Описание')->textarea(['rows' => 7]);
        ?>
    <div class="row form-group">
        <div class="col-md-5 form-group">
            <?= $form->field($model, 'product_category')->dropDownList(['футболки', 'джинсы', 'рубашки', 'толстовки' ])->label('Выберите Категорию');?>
            <?= $form->field($model, 'product_color')->dropDownList(['темные', 'светлые', 'с принтом'])->label('Выберите цвет');?>
            <?= $form->field($model, 'product_price')->textInput(['type' => 'number'])->label('Цена товара');?>
        </div>
        <div class="col-md-2"></div>
        <div class="col-md-5 form-group">
            <?= $form->field($model, 'product_type')->dropDownList(['мужские', 'женские'])->label('Выберите тип'); ?>
            <?= $form->field($model, 'product_status')->dropDownList(['В наличии','Временно отсутствует', 'Доступен для заказа', 'Не доступен для заказа'])->label('Выберите статус товара');?>
            <?= $form->field($model, 'imageFile')->fileInput()->label('Изображение товара');?>
        </div>
    </div>
        <?= Html::submitButton('Отправить', ['class' => 'btn btn-success'])?>
    <?php ActiveForm::end() ?>
</div>