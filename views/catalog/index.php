<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
$domain = Url::base(true);
$count = 0;
?>

<div class="container-fluid">
    <?php $form = ActiveForm::begin(['options'=>['class'=>'form-horizontal', 'id'=>'product', 'enctype' => 'multipart/form-data', 'method' =>'post']]) ?>
    <div class="row">
        <div class="col-xs-9">
            <?= $form->field($search, 'search_word')->label('Поиск'); ?>
        </div>
        <div class="col-xs-3" style="padding-top: 1.8em;">
            <?= Html::submitButton('Искать', ['class' => 'btn btn-success'])?>
            <?= Html::resetButton('Сброс', ['class' => 'btn btn-default'])?>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-3" >
            <?= $form->field($search, 'category', ['options'=>['style'=>['padding-left: 1px; padding-right: 1px;']]])->dropDownList(['Выберите категорию', 'футболки', 'джинсы', 'рубашки', 'толстовки'])->label('Выберите Категорию'); ?>
        </div>
        <div class="col-xs-3" >
            <?= $form->field($search, 'color', ['options'=>['style'=>['padding-left: 1px; padding-right: 1px;']]])->dropDownList(['Выберите цвет', 'темные', 'светлые', 'с принтом'])->label('Выберите цвет');?>
        </div>
        <div class="col-xs-3" >
            <?= $form->field($search, 'type', ['options'=>['style'=>['padding-left: 1px; padding-right: 1px;']]])->dropDownList(['Выберите тип', 'мужские', 'женские'])->label('Выберите тип'); ?>
        </div>
        <div class="col-xs-3" >
            <?= $form->field($search, 'status', ['options'=>['style'=>['padding-left: 1px; padding-right: 1px;']]])->dropDownList(['Выберите статус', 'В наличии','Временно отсутствует', 'Доступен для заказа', 'Не доступен для заказа'])->label('Выберите статус товара');?>
        </div>
    </div>

        <?php ActiveForm::end() ?>
</div>

<div>
<table class="table-responsive table-bordered text-center" style="margin-top: 20px">
    <thead>
        <tr>
            <th>№</th>
            <th>Наименование</th>
            <th>Категория</th>
            <th>Описание</th>
            <th>Тип</th>
            <th>Цвет</th>
            <th>Заказ</th>
            <th>Цена за единицу</th>
            <th>Изображение</th>
            <th></th>
            <th></th>
            <th></th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($catalog as $key => $item): ?>
        <tr>
            <td><?php $count++; echo $count ?></td>
            <td class="text-left"><?= $item['product_title']?></td>
            <td><?= $item['product_category_id']?></td>
            <td class="text-left"><?= $item['product_desc']?></td>
            <td><?= $item['product_type']?></td>
            <td><?= $item['product_color']?></td>
            <td><?= $item['product_status']?></td>
            <td><?= $item['product_price']?></td>
            <td><?= Html::img($domain . '/web/img/' . $item['product_img'], ['class' => 'img-thumbnail']);?></td>
            <td>
                <a type="button" class="btn btn-default" href="<?=Url::toRoute('product/index/?id='. $item['product_id']); ?>">
                    <span class="glyphicon glyphicon-refresh"></span>
                </a>
            </td>
            <td>
                <a type="button" class="btn btn-default" href="<?=Url::toRoute('card/index/?id='. $item['product_id']); ?>">
                    <span class="glyphicon glyphicon-search"></span></a>
            </td>
            <td>
                <a type="button" class="btn btn-default add_card" href="<?= Url::to(['cart/add', 'id' => $item['product_id']]) ?>" data-id="<?=$item['product_id']?>">
                    <span class="glyphicon glyphicon-shopping-cart"></span>Купить</a>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
</div>
