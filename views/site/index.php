<?php
use yii\helpers\Url;
use yii\helpers\Html;
use app\widgets\TopSix;
use app\widgets\Carousel;

$domain = yii\helpers\Url::base(true);

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">
    <!-- карусель   -->
    <?php echo Carousel::widget(); ?>

    <div class="container content">
        <ul class="nav nav-pills nav-stacked col-md-2">
            <li class="active"><a href="#tab_a" data-toggle="pill">Футболки</a></li>
            <li><a href="#tab_b" data-toggle="pill">Джинсы</a></li>
            <li><a href="#tab_c" data-toggle="pill">Рубашки</a></li>
            <li><a href="#tab_d" data-toggle="pill">Худи</a></li>
        </ul>
        <div class="tab-content col-md-10">
            <div class="tab-pane active" id="tab_a">
                <div class="row">
                    <?php echo TopSix::widget(['category' => '1']); ?>
                </div>
            </div>
            <div class="tab-pane" id="tab_b">
                <div class="row">
                    <?php echo TopSix::widget(['category' => '2']); ?>
                </div>
            </div>
            <div class="tab-pane" id="tab_c">
                <div class="row">
                    <?php echo TopSix::widget(['category' => '3']); ?>
                </div>
            </div>
            <div class="tab-pane" id="tab_d">
                <div class="row">
                    <?php echo TopSix::widget(['category' => '4']); ?>
                </div>
            </div>
        </div>
    </div>


</div>
