<div class="container">
    <?php
    use yii\helpers\Url;
    use yii\helpers\Html;
    use yii\widgets\ActiveForm;
    $domain = Url::base(true);
    ?>
    <div id="inside">
        <?php if(!empty($session['cart'])): ?>
        <div class="table-responsive">
            <table class="table table-hover table-stripped">
                <thead>
                <tr>
                    <th>Фото</th>
                    <th>Наименование</th>
                    <th>Кол-во</th>
                    <th>Цена</th>
                    <th>Сумма</th>
                    <th><span class="glyphicon gliphicon-remove" aria-hidden="true"></span></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($session['cart'] as $id => $item): ?>
                    <tr>
                        <td><?=Html::img($domain.'/web/img/'.$item['product_img']) ?></td>
                        <td><a href="<?= Url::to(['card/index', 'id' => $id]); ?>"><?=$item['product_title'] ?></a></td>
                        <td><?=$item['qty'] ?></td>
                        <td><?=$item['product_price'] ?></td>
                        <td><?=$item['product_price']*$item['qty'] ?></td>
                        <td><span class="glyphicon glyphicon-remove text-danger del-item" aria-hidden="true" data-id="<?=$id; ?>"></span></td>
                    </tr>
                <?php endforeach; ?>
                <tr>
                    <td colspan="4">Итого: </td>
                    <td><?= $session['cart.qty'] ?></td>
                    <td></td>
                </tr>
                <tr>
                    <td colspan="4">Сумма: </td>
                    <td><?= $session['cart.sum'] ?></td>
                    <td></td>
                </tr>
                </tbody>
            </table>
        </div>
        <?php else: ?>
            <h3>Корзина пуста</h3>
        <?php endif;?>
    </div>

        <?php $form = ActiveForm::begin()?>
        <?= $form->field($order, 'name')?>
        <?= $form->field($order, 'email')?>
        <?= $form->field($order, 'phone')?>
        <?= $form->field($order, 'address')?>
        <?= Html::submitButton('Заказать', ['class' => 'btn btn-success']) ?>
        <?php ActiveForm::end() ?>

</div>

