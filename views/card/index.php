<?php
use yii\helpers\Html;
use yii\helpers\Url;
$domain = Url::base(true);

?>
<div class="container">
    <div class="row">
        <div class="col-md-6">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12"><?= Html::img($domain.'/web/img/'.$query['product_img'], ['class' => 'img-responsive']); ?></div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <h3><?=$query['product_title'] ?></h3>
            <p>Описание: <?= $query['product_desc']?></p>
            <h3><?= $query['product_price']?> руб.</h3>
            <div class="row" style="margin-top: 50px;">
                <div class="col-sm-4 form-group" style="text-align: center;">
                    <label for="qty">Количество</label>
                    <input type="text" class="form-control" id="qty" value="1">
                </div>
                <div class="col-sm-4 form-group" style="text-align: center;">
                    <a type="button" style="margin-top: 1.7em;" class="btn btn-success add_card" href="#" data-id="<?=$query['product_id']?>">Добавить в заказ
                    </a>
                </div>
                <div class="col-sm-4 form-group" style="text-align: center;">
                    <a type="button" style="margin-top: 1.7em;" class="btn btn-primary" href="<?=Url::toRoute('catalog/index'); ?>">
                        <span class="glyphicon glyphicon-share-alt"></span>Continue shopping
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

