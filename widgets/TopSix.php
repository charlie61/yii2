<?php


namespace app\widgets;

use yii\base\Widget;
use yii\db\Query;


class TopSix extends Widget{

    public $category;

    public function init(){
        parent::init();
        if($this->category === null) $this->category = '1';
    }

    public function run() {

        $rows = $this->getSix($this->category);
        return $this->render('top', compact('rows'));
    }
    public function getSix($cat){

        $query = (new Query())->select('*')
            ->from('products')
            ->where(['product_category_id' => $cat])
            ->limit(4)
            ->all();
        return $query;
    }
}