<!--<h1>--><?//=$category?><!--, Hello</h1>-->

<?php
use yii\helpers\Html;
use yii\helpers\Url;
$domain = Url::base(true);
//debug($rows);
//foreach ($rows as )

?>
<?php foreach ($rows as $key => $item): ?>
<div class="col-sm-3">

    <div class="col-item one-card">
        <div class="photo">
            <?= Html::img($domain.'/web/img/'.$item['product_img'], ['class' => 'img-responsive']); ?>
        </div>
        <div class="info">
            <div class="row">
                <div class="price col-md-6">
                    <h5><?= $item['product_title']?></h5>
                    <h5 class="price-text-color">
                        <?= $item['product_price']?></h5>
                </div>
                <div class="rating hidden-sm col-md-6">
                </div>
            </div>
            <div class="separator clear-left">
                <p class="btn-add">
                    <i class="fa fa-shopping-cart"></i><a href="<?= Url::to(['cart/add', 'id' => $item['product_id']]) ?>" data-id="<?=$item['product_id']?>" class="hidden-sm add_card">Add to cart</a></p>
                <p class="btn-details">
                    <i class="fa fa-list"></i><a href="<?=Url::toRoute('card/index/?id='. $item['product_id']); ?>" class="hidden-sm">More details</a></p>
            </div>
            <div class="clearfix">
            </div>
        </div>
    </div>
</div>
<?php endforeach; ?>
