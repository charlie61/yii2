<?php
use yii\helpers\Url;
use yii\helpers\Html;

$domain = yii\helpers\Url::base(true);

?>

<div class="container">
    <div class="row">
        <div class="row">
            <div class="col-md-9">
                <h3>
                    Carousel Product Cart Slider</h3>
            </div>
            <div class="col-md-3">
                <!-- Controls -->
                <div class="controls pull-right hidden-xs">
                    <a class="left fa fa-chevron-left btn btn-primary" href="#carousel-example-generic"
                       data-slide="prev"></a><a class="right fa fa-chevron-right btn btn-primary" href="#carousel-example-generic"
                                                data-slide="next"></a>
                </div>
            </div>
        </div>
        <div id="carousel-example-generic" class="carousel slide hidden-xs" data-ride="carousel">
            <!-- Wrapper for slides -->
            <div class="carousel-inner">
                <div class="item active">
                    <div class="row">
                    <?php foreach ($category as $item): ?>
                            <div class="col-sm-3">
                            <div class="col-item">
                                <div class="photo">
                                    <?= Html::img($domain.'/web/img/' . $item->category_img, ['class' => 'img-responsive']); ?>
                                </div>
                                <div class="info">
                                    <div class="row">
                                        <div class="price col-md-6">
                                            <h5><?= $item->category_title ?></h5>
                                        </div>
                                    </div>
                                    <div class="separator clear-left">
                                    </div>
                                    <div class="clearfix">
                                    </div>
                                </div>
                            </div>
                        </div>
                      <?php  endforeach?>
                    </div>
                </div>
                <div class="item">
                    <div class="row">
                        <?php foreach ($category as $item): ?>
                        <div class="col-sm-3">
                            <div class="col-item">
                                <div class="photo">
                                    <?= Html::img($domain.'/web/img/' . $item->category_img, ['class' => 'img-responsive']); ?>
                                </div>
                                <div class="info">
                                    <div class="row">
                                        <div class="price col-md-6">
                                            <h5><?= $item->category_title ?></h5>
                                        </div>
                                    </div>
                                    <div class="separator clear-left">
                                    </div>
                                    <div class="clearfix">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php  endforeach?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid text-left">

</div>
