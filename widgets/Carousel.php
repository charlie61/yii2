<?php


namespace app\widgets;

use yii\base\Widget;
use app\models\Category;

class Carousel extends Widget{
    public $category;

    public function run(){
        $category = $this->getCategory();
        return $this->render('carousel', compact('category'));
    }

    public function getCategory(){
        $cats = Category::find()->all();
        return $cats;
    }

}