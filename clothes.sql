-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1
-- Время создания: Окт 25 2019 г., 12:39
-- Версия сервера: 10.1.36-MariaDB
-- Версия PHP: 7.0.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `clothes`
--

-- --------------------------------------------------------

--
-- Структура таблицы `categories`
--

CREATE TABLE `categories` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `category_title` varchar(255) NOT NULL,
  `category_parent_id` tinyint(3) UNSIGNED NOT NULL,
  `category_description` varchar(255) DEFAULT NULL,
  `category_img` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `categories`
--

INSERT INTO `categories` (`id`, `category_title`, `category_parent_id`, `category_description`, `category_img`) VALUES
(1, 'футболки', 0, 'Лучшие футболки от Майки.ru!', 't-shirt4.jpg'),
(2, 'джинсы', 0, 'Самые свежие поступления от Wrangler-Russia!', 'джинсы.jpg'),
(3, 'рубашки', 0, 'Отличные Мужские рубашки только у нас!', 'рубашка.jpg'),
(4, 'толстовки', 0, 'Толстовки на все случаи жизни! Бери!', 'толстовка.jpg');

-- --------------------------------------------------------

--
-- Структура таблицы `products`
--

CREATE TABLE `products` (
  `product_id` mediumint(8) UNSIGNED NOT NULL,
  `product_category_id` tinyint(2) UNSIGNED NOT NULL,
  `product_title` varchar(255) NOT NULL,
  `product_price` float(10,2) UNSIGNED NOT NULL DEFAULT '0.00',
  `product_status` tinyint(1) UNSIGNED NOT NULL,
  `product_type` tinyint(1) UNSIGNED NOT NULL,
  `product_color` tinyint(1) UNSIGNED NOT NULL,
  `product_desc` varchar(255) NOT NULL,
  `product_img` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `products`
--

INSERT INTO `products` (`product_id`, `product_category_id`, `product_title`, `product_price`, `product_status`, `product_type`, `product_color`, `product_desc`, `product_img`) VALUES
(1, 2, 'Джинсы черные новое поступление', 500.00, 0, 1, 0, 'Новое поступление весенней коллекции', '17ff7b4ba684c1eaa40a074a3851fa7f.jpg'),
(2, 2, 'Джинсы черные поступление 2020', 550.00, 2, 1, 0, 'Новое поступление осенней коллекции', 'ba16707a136f96e841a4ccfc6cadac50.jpg'),
(3, 2, 'Джинсы плюс-сайз', 600.00, 2, 1, 1, 'Новое поступление осенней коллекции', 'b45202211378d496c267ecd27fe1c497.jpg'),
(4, 2, 'Джинсы плюс-сайз', 400.00, 2, 1, 1, 'Новое поступление осенней коллекции', '70eda5a3824454089fb05a5e35f8f7c4.jpg'),
(5, 2, 'Джинсы офис', 900.00, 2, 1, 1, 'Новое поступление осенней коллекции', 'd73f8867d91ff5a2b8d20019bfaf873e.jpg'),
(6, 1, 'Футболка Морячок', 1200.00, 0, 1, 2, 'Новое поступление осенней коллекции', '66f95bb7bc175179d057b6fe58338a44.jpg'),
(7, 1, 'Футболка Морячок', 1200.00, 0, 1, 2, 'Новое поступление осенней коллекции', '66f95bb7bc175179d057b6fe58338a44.jpg'),
(8, 1, 'Футболка Морячок', 1200.00, 0, 1, 2, 'Новое поступление осенней коллекции', '66f95bb7bc175179d057b6fe58338a44.jpg'),
(9, 2, 'Джинсы хипстерские', 800.00, 3, 1, 1, 'Новое поступление', 'c002bb0c625d33db371ef34028428ea5.jpg'),
(10, 2, 'Джинсы темные', 1500.00, 1, 1, 0, 'Новое поступление', 'adc05abfc6a8733bc06b00a4ad665542.jpg'),
(11, 2, 'Джинсы женские', 900.00, 0, 1, 1, 'Модные джинсы', 'b9ceedad2717eb0a3130eb973a3a9dbf.jpg'),
(12, 2, 'Джинсы серые', 500.00, 0, 0, 0, 'Серые повседневные джинсы', '756493d3f8a79f0d4b02a77f41792e6b.jpg'),
(13, 2, 'джинсы городские', 700.00, 2, 0, 0, 'черные джинсы новая коллекция', '2f2c34a8d970e8d0a22d9ab3608664e9.jpg'),
(14, 2, 'Джинсы модные', 2500.00, 0, 0, 1, 'Куча дырок, самые модные', 'd2ebf960d13de58e31c2f00676058635.jpg'),
(15, 3, 'Рубашка салат', 400.00, 0, 1, 1, 'рубашка салатового цвета', 'd9bbeffa814f40bfa354302fd08cee1a.jpg'),
(16, 3, 'Рубашка в клетку', 800.00, 0, 1, 1, 'Офисный вариант', 'fbb8394513a106a5cb92ef91e6e33fe7.jpg'),
(17, 3, 'Рубашка 2020', 900.00, 0, 1, 1, 'Новая коллекция 2020', 'b0876750f65612960110ba39f28154fd.jpg'),
(18, 3, 'Рубашка Дровосек', 1200.00, 0, 0, 0, 'Крупная клетка', '1c6a5181bb5b1afb5d0c1bb87abdcd6e.jpg'),
(19, 3, 'рубашка обычная', 300.00, 2, 0, 1, 'магия клетки купить', 'b5281a55d0546ff70bdc340a9e3f1635.jpg'),
(20, 3, 'Рубашка 2020', 1300.00, 0, 0, 0, 'танцевать в клубе узоры покупай', '6f055be2b627b37049ec452d4cc61f22.jpg'),
(21, 3, 'Рубашка бизнес', 2000.00, 0, 0, 1, 'Бизнес коллекция 2020', '8627d204264197202f5730209dcae862.jpg'),
(22, 4, 'Толстовка женская', 2000.00, 0, 1, 1, 'Розовая', 'e493b4878272a878e7c3c18bce041728.jpg'),
(23, 4, 'Толстовка старый побегушник', 2300.00, 0, 0, 0, 'Теплая толстовка', 'e6d7ae1407153611409e622c6ea0573a.jpg'),
(24, 4, 'Толстовка mike', 1000.00, 0, 0, 0, 'Хорошая толстовка', 'e82268b7dc5aa3cce8d747391745b320.jpg'),
(25, 4, 'толстовка с принтом', 1100.00, 2, 0, 2, 'коллекция 2020', '01650c9bf4466f7331387e1ce3543974.jpg'),
(26, 4, 'Толстовка Папай', 1000.00, 0, 0, 1, 'коллекция 2020', 'a5b4f48d4c8901b011fb369b0413fdd2.jpg'),
(27, 1, 'Футболка черная', 400.00, 0, 0, 0, 'хлопок', '0c4d105fa2b1a7d920dbbd32c915c166.jpg'),
(28, 1, 'Футболка чужой', 300.00, 0, 0, 2, 'Футболка с принтом', 'c650b049e8ac8931912f360967a02e58.jpg'),
(29, 1, 'Футболка скелет в цветах', 800.00, 2, 1, 2, 'Футболка с принтом', '0eaf8d0b20566749fed085e500bb581f.jpg'),
(30, 1, 'Футболка Череп', 400.00, 2, 1, 2, 'Хлопок', 'e62d2d6693150b480e05cb61e1ef2d9b.jpg'),
(31, 1, 'Футболка черная', 600.00, 0, 0, 0, 'Материал хлопок', '0c4d105fa2b1a7d920dbbd32c915c166.jpg'),
(32, 1, 'Футболка черная', 900.00, 0, 0, 0, 'Вискоза', 'd2fa47cd545b44a6a02591b4e78bd4ce.jpg');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`product_id`),
  ADD KEY `product_category` (`product_category_id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `categories`
--
ALTER TABLE `categories`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `products`
--
ALTER TABLE `products`
  MODIFY `product_id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
