<?php


namespace app\controllers;

use app\models\Catalog;
use yii\web\Controller;
use yii\helpers\Html;
use Yii;
use yii\db\Query;

class CatalogController extends Controller {


    public function actionIndex() {
        $search = new Catalog();
        if($search->load(Yii::$app->request->post())) {
            if ($search->validate()){
                $search_word = Html::encode($search->search_word);
                $category = Html::encode($search->category);
                if($category == '0'){
                    $category = '';
                }
                $color = $this->setSearchValue(Html::encode($search->color));
                $type = $this->setSearchValue(Html::encode($search->type));
                $status = $this->setSearchValue(Html::encode($search->status));
                $price = $this->setSearchValue(Html::encode($search->price));

                $catalog = (new Query())
                    ->select('*')
                    ->from('products')
                    ->where(['and', ['like','product_title', $search_word], ['like','product_category_id', $category], ['like','product_color', $color], ['like','product_type', $type], ['like','product_status', $status]])
                    ->all();
                $catalog = $this->repackCatalog($catalog);
            }
        }else {
            $catalog = (new Query())
                ->select('*')
                ->from('products')
                ->all();
            $catalog = $this->repackCatalog($catalog);
        }


        return $this->render('index', compact('catalog', 'search', 'search_word', 'category', 'color', 'type', 'status',  'price' ));
    }

    public function setSearchValue($col){
        switch ($col){
            case '1':
                $col = '0';
                break;
            case '2':
                $col = '1';
                break;
            case '3':
                $col = '2';
                break;
            case '4':
                $col = '3';
                break;
            default:
                $col = '';

        }
        return $col;
    }
    public function repackCatalog($arr){
        $repack = [];
        foreach ($arr as $key =>$item){
            switch ($item['product_category_id']) {
                case '1':
                    $item['product_category_id'] = 'Футболки';
                    break;
                case '2':
                    $item['product_category_id'] = 'Джинсы';
                    break;
                case '3':
                    $item['product_category_id'] = 'Рубашки';
                    break;
                case '4':
                    $item['product_category_id'] = 'Толстовки';
                    break;
            }
            switch ($item['product_status']) {
                case'0':
                    $item['product_status'] = 'В наличии';
                    break;
                case'1':
                    $item['product_status'] = 'Временно отсутствует';
                    break;
                case'2':
                    $item['product_status'] = 'Доступен для заказа';
                    break;
                case'3':
                    $item['product_status'] = 'Не доступен для заказа';
                    break;
            }
            if ($item['product_type'] == '0') {
                $item['product_type'] = 'Мужкая';
            }
            if ($item['product_type'] == '1') {
                $item['product_type'] = 'Женская';
            }
            switch ($item['product_color']) {
                case'0':
                    $item['product_color'] = 'Темные';
                    break;
                case'1':
                    $item['product_color'] = 'Светлые';
                    break;
                case'2':
                    $item['product_color'] = 'С принтом';
                    break;
            }
            $repack[] = $item;
        }
        return $repack;
    }

}