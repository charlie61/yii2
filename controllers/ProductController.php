<?php


namespace app\controllers;

use yii\helpers\Html;
use yii\web\Controller;
use app\models\ProductForm;
use Yii;
use yii\web\UploadedFile;

class ProductController extends Controller{

    public function actionIndex($id = null) {
        $model = new ProductForm();
        if($model->load(Yii::$app->request->post())){
            $model->imageFile = UploadedFile::getInstance($model, 'imageFile');
            $name = Html::encode($model->name);
            $desc = Html::encode($model->desc);
            $product_category = Html::encode($model->product_category);
            $product_status = (int)Html::encode($model->product_status);
            switch($product_category){
                case '0' :
                    $product_category = 1;
                    break;
                case '1' :
                    $product_category = 2;
                    break;
                case '2':
                    $product_category = 3;
                    break;
                case '3':
                    $product_category = 4;
                    break;
            }
            $product_color = (int)Html::encode($model->product_color);
            $product_type = (int)Html::encode($model->product_type);
            $product_price = (float)Html::encode($model->product_price);
            $image_name =  md5_file($model->imageFile->tempName) . '.' . $model->imageFile->extension;
            $params = ['product_category_id' => $product_category,
                'product_title' => $name,
                'product_price' => $product_price,
                'product_status' => $product_status,
                'product_type' => $product_type,
                'product_color' => $product_color,
                'product_desc' => $desc,
                'product_img' => $image_name
                ];
            if ($model->validate()){
                $model->imageFile->saveAs('img/'. $image_name);
                if(!$id){
                    Yii::$app->db->createCommand()->insert('products', $params)->execute();
                    Yii::$app->session->setFlash('success', 'Данные приняты');
                    return $this->refresh();
                }else{
                    $row = [
                      'product_id' =>$id
                    ];
                    Yii::$app->db->createCommand()->update('products', $params, $row)->execute();
                    Yii::$app->session->setFlash('success', 'Данные изменены');
                    return $this->refresh();
                }

            }else {
                Yii::$app->session->setFlash('error', 'Ошибка!');
                return $this->refresh();
            }
        }
        return $this->render('index', compact('id', 'model'));
    }
}