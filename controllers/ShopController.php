<?php


namespace app\controllers;

use yii\web\Controller;
use Yii;


class ShopController extends Controller {
    public function actionIndex() {
        if(Yii::$app->request->isAjax){
            debug($_POST);
            return 'test';
        }

        return $this->render('index');
    }

}