<?php


namespace app\controllers;

use yii\web\Controller;
use yii\db\Query;


class CardController extends Controller {

    public function actionIndex($id = null) {

        if(!$id){
            $query = null;
        }else{
            $query = (new Query())
                ->select('*')
                ->from('products')
                ->where(['product_id' => $id])
                ->one();

        }

        return $this->render('index', compact('id', 'query'));
    }
}