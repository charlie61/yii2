<?php

namespace app\models;
use yii\db\ActiveRecord;


class Cart extends ActiveRecord {

    public static function tableName()
    {
        return 'products';
    }


    public function addToCart($product, $qty = 1){
        if(isset($_SESSION['cart'][$product->product_id])){
            $_SESSION['cart'][$product->product_id]['qty'] +=$qty;
        }else {
            $_SESSION['cart'][$product->product_id] = [
                'qty' => $qty,
                'product_title' => $product->product_title,
                'product_price' => $product->product_price,
                'product_img' => $product->product_img
            ];
        }
        $_SESSION['cart.qty'] = isset($_SESSION['cart.qty']) ? $_SESSION['cart.qty']+$qty : $qty;
        $_SESSION['cart.sum'] = isset($_SESSION['cart.sum']) ? $_SESSION['cart.sum']+$qty*$product->product_price : $qty*$product->product_price;

    }

    public function recalc($id){
        if(!isset($_SESSION['cart'][$id])) return false;

        $qtyMinus = $_SESSION['cart'][$id]['qty'];
        $sumMinus = $_SESSION['cart'][$id]['qty']*$_SESSION['cart'][$id]['product_price'];
        $_SESSION['cart.qty'] -= $qtyMinus;
        $_SESSION['cart.sum'] -= $sumMinus;
        unset($_SESSION['cart'][$id]);
    }
}