<?php


namespace app\models;

use yii\db\ActiveRecord;


class ProductForm extends ActiveRecord {
    public $name;
    public $desc;
    public $product_category;
    public $product_color;
    public $product_type;
    public $product_status;
    public $product_price;
    public $imageFile;

    public function rules(){
        return [
            ['name', 'required'],
            ['name', 'string', 'min' => 3, 'max' => 100],
            ['desc', 'required'],
            ['desc', 'string', 'min' => 5, 'max' => 200],
            ['product_category', 'required'],
            ['product_color', 'required'],
            ['product_type', 'required'],
            ['product_status', 'required'],
            ['product_price', 'required'],
            [['imageFile'], 'file', 'extensions' => 'png, jpg'],
        ];
    }
}