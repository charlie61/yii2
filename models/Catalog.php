<?php


namespace app\models;

use yii\db\ActiveRecord;

class Catalog extends ActiveRecord {
    public $search_word;
    public $category;
    public $color;
    public $type;
    public $status;
    public $price;

    public static function tableName()
    {
        return 'products';
    }

    public function rules(){
        return [
            ['search_word', 'trim'],
            ['category', 'trim'],
            ['color', 'trim'],
            ['type', 'trim'],
            ['status', 'trim'],
            ['price', 'trim'],
        ];
    }

}